<?php
function tentukan_nilai($number)
{
    if ($number<=100 && 85<=$number){
        echo $number. " = Sangat Baik<br>";
    } else if ($number<85 && 70<=$number){
        echo $number. " = Baik<br>";
    } else if ($number<70 && 60<=$number){
        echo $number. " = Cukup<br>";
    } else if ($number<60){
        echo $number. " = Kurang<br>";
    }
    //  kode disini
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>